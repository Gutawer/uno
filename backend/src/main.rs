use std::time::{Duration, Instant};
use std::fs::File;
use std::io::BufReader;
use std::collections::HashMap;

use serde::{Deserialize, Serialize};
use rand::{self, seq::SliceRandom, rngs::ThreadRng};
use actix::{Actor, StreamHandler};
use actix_web::{
	web, App, Error, HttpRequest, HttpResponse, HttpServer, 
	middleware::{Logger, NormalizePath}
};
use actix_web_actors::ws;
use rustls::internal::pemfile::{certs, pkcs8_private_keys};
use rustls::{NoClientAuth, ServerConfig};
use actix::prelude::*;

use log::*;

mod server;

#[derive(Deserialize, Serialize, Debug, Copy, Clone, Eq, PartialEq)]
enum CardColor {
	Red,
	Green,
	Yellow,
	Blue
}

#[derive(Deserialize, Serialize, Debug, Copy, Clone)]
#[serde(tag = "type")]
enum Card {
	Number { color: CardColor, num: u8, which: u8 },
	Reverse { color: CardColor, which: u8 },
	Plus2 { color: CardColor, which: u8 },
	Skip { color: CardColor, which: u8 },
	Wild { which: u8 },
	WildDrawFour { which: u8 }
}

impl Card {
	fn get_color(&self) -> Option<CardColor> {
		match *self {
			Self::Number { color, .. }  => Some(color),
			Self::Reverse { color, .. } => Some(color),
			Self::Plus2 { color, .. }   => Some(color),
			Self::Skip { color, .. }    => Some(color),

			_ => None
		}
	}

	fn type_compatible(&self, new: &Card) -> bool {
		match (*self, *new) {
			(Self::Number { num: num0, .. }, Self::Number { num: num1, .. }) => num0 == num1,
			(Self::Reverse { .. }, Self::Reverse { .. }) => true,
			(Self::Plus2 { .. }, Self::Plus2 { .. }) => true,
			(Self::Skip { .. }, Self::Skip { .. }) => true,

			(_, Self::Wild { .. }) => true,
			(_, Self::WildDrawFour { .. }) => true,

			_ => false
		}
	}

	fn apply_card_action(&self, room: &mut server::Room, rng: &mut ThreadRng, player_data: Option<(usize, server::PlayCard)>) {
		if let server::RoomState::Playing(ref mut s) = room.state {
			match *self {
				Self::Reverse { .. } => {
					s.pos_direction = !s.pos_direction;
					for _ in 0..(if s.player_hands.len() > 2 { 2 } else { 1 }) {
						room.advance_player(false);
					}
				}
				Self::Plus2 { .. } => {
					if let Some(ref mut t) = s.stack_target {
						if !t.plus_four {
							t.stack_amount += 1;
						} else {
							error!("A +2 was stacked on top of a +4");
						}
					} else {
						s.stack_target = Some(server::StackTarget {
							plus_four: false,
							stack_amount: 1
						});
					}
					let hand = s.player_hands.get_mut(&room.player_order[s.cur_player]).unwrap();
					if !(room.house_rules.stackable_plus && hand.iter().any(|c| matches!(c, Card::Plus2 { .. }))) {
						deal(&mut s.deck, &mut s.discard, hand, s.stack_target.unwrap().stack_amount * 2, rng);
						s.stack_target = None;
						room.advance_player(false);
					}
				}
				Self::Skip { .. } => {
					room.advance_player(false);
				}
				Self::WildDrawFour { .. } => {
					if let Some(ref mut t) = s.stack_target {
						if t.plus_four {
							t.stack_amount += 1;
						} else {
							error!("A +4 was stacked on top of a +2");
						}
					} else {
						s.stack_target = Some(server::StackTarget {
							plus_four: true,
							stack_amount: 1
						});
					}
					let hand = s.player_hands.get_mut(&room.player_order[s.cur_player]).unwrap();
					if !(room.house_rules.stackable_plus && hand.iter().any(|c| matches!(c, Card::WildDrawFour { .. }))) {
						deal(&mut s.deck, &mut s.discard, hand, s.stack_target.unwrap().stack_amount * 4, rng);
						s.stack_target = None;
						room.advance_player(false);
					}
				}
				Self::Number { num: 0, .. } if room.house_rules.seven_zero => {
					let mut new_player_hands = vec![];
					for (i, cur) in room.player_order.iter().enumerate() {
						let next_i = if s.pos_direction {
							room.player_order.len() + i + 1
						} else {
							room.player_order.len() + i - 1
						} % room.player_order.len();
						let next = room.player_order[next_i];
						new_player_hands.push((next, s.player_hands[cur].clone()));
					}
					s.player_hands.clear();
					s.player_hands.extend(new_player_hands.into_iter());
				}
				Self::Number { num: 7, .. } if room.house_rules.seven_zero => {
					if let Some((p, msg)) = player_data {
						let player_id = room.player_order[p];

						let player_hand = s.player_hands[&player_id].clone();
						let other_index = msg.seven_target.unwrap_or_else(|| {
							error!("Client sent no 7 target with 7-0 on");
							if p == 0 { 1 } else { 0 }
						});
						let other_index = if other_index == p {
							error!("Client sent themselves as a 7 target with 7-0 on");
							if p == 0 { 1 } else { 0 }
						} else { other_index };

						let other_id =
							room.player_order.get(other_index).cloned().unwrap_or_else(|| {
								error!("Client sent an out of bounds 7 target with 7-0 on");
								if p == 0 { 1 } else { 0 }
							});

						let other_hand = s.player_hands[&other_id].clone();

						s.player_hands.get_mut(&player_id).unwrap().clear();
						s.player_hands.get_mut(&player_id).unwrap().extend(other_hand);

						s.player_hands.get_mut(&other_id).unwrap().clear();
						s.player_hands.get_mut(&other_id).unwrap().extend(player_hand);
					}
				}
				_ => {}
			}
		}
	}
}

fn get_deck(rng: &mut ThreadRng) -> Vec<Card> {
	let mut v = vec![];
	use CardColor::*;
	use Card::*;
	for &c in [Red, Green, Yellow, Blue].iter() {
		v.push(Number { color: c, num: 0, which: 0 });
		for i in 1..=9 {
			for w in 0..2 { v.push(Number { color: c, num: i, which: w }); }
		}
		for w in 0..2 { v.push(Reverse { color: c, which: w }); }
		for w in 0..2 { v.push(Plus2 { color: c, which: w }); }
		for w in 0..2 { v.push(Skip { color: c, which: w }); }
	}
	for w in 0..4 { v.push(Wild { which: w }); }
	for w in 0..4 { v.push(WildDrawFour { which: w }); }

	v.shuffle(rng);

	v
}

fn deal(
	deck: &mut Vec<Card>, discard: &mut Vec<Card>, hand: &mut Vec<Card>,
	amount: usize, rng: &mut ThreadRng
) {
	for _ in 0..amount {
		let card = {
			match deck.pop() {
				Some(c) => c,
				None => {
					deck.extend(discard.drain(0..=(discard.len() - 2)));
					deck.shuffle(rng);
					match deck.pop() {
						Some(c) => c,
						None => { return; }
					}
				}
			}
		};
		hand.push(card);
	}
}

fn score(hand: &[Card]) -> u32 {
	hand
		.iter()
		.map(|c| match c {
			Card::Number { num, .. } => *num as u32,
			Card::Reverse { .. } | Card::Plus2 { .. } | Card::Skip { .. } => 20,
			Card::Wild { .. } | Card::WildDrawFour { .. } => 50
		})
		.sum()
}

#[derive(Debug, Clone, Default)]
struct UnoWsData {
	id: usize
}

const HEARTBEAT_INTERVAL: Duration = Duration::from_secs(5);
const CLIENT_TIMEOUT: Duration = Duration::from_secs(20);

struct UnoWs {
	addr: Addr<server::UnoServer>,
	data: UnoWsData,
	hb: Instant
}

impl UnoWs {
	fn hb(&self, ctx: &mut ws::WebsocketContext<Self>) {
		ctx.run_interval(HEARTBEAT_INTERVAL, move |act, ctx| {
			if Instant::now().duration_since(act.hb) > CLIENT_TIMEOUT {
				info!("Websocket {} heartbeat failed, disconnecting", act.data.id);

				ctx.stop();

				return;
			}

			//debug!("Pinging {}", act.data.id);
			ctx.ping(b"");
		});
	}
}

impl Actor for UnoWs {
	type Context = ws::WebsocketContext<Self>;

	fn started(&mut self, ctx: &mut Self::Context) {
		self.hb(ctx);

		let addr = ctx.address();
		self.addr
			.send(server::Connect {
				addr: addr.recipient()
			})
			.into_actor(self)
			.then(|res, act, ctx| {
				match res {
					Ok(res) => act.data.id = res,
					_ => ctx.stop(),
				}
				fut::ready(())
			})
			.wait(ctx)
	}

	fn stopping(&mut self, _: &mut Self::Context) -> Running {
		self.addr.do_send(server::Disconnect { ws: self.data.clone() });
		Running::Stop
	}
}

impl Handler<server::Message> for UnoWs {
	type Result = ();

	fn handle(&mut self, msg: server::Message, ctx: &mut Self::Context) {
		ctx.text(msg.0);
	}
}

impl StreamHandler<Result<ws::Message, ws::ProtocolError>> for UnoWs {
	fn handle(
		&mut self,
		msg: Result<ws::Message, ws::ProtocolError>,
		ctx: &mut Self::Context,
	) {
		let msg = match msg {
			Err(_) => {
				ctx.stop();
				return;
			}
			Ok(msg) => msg,
		};

		match msg {
			ws::Message::Text(text) => {
				let v: serde_json::Value = {
					let v = serde_json::from_str(&text);
					match v {
						Ok(v) => v,
						_ => {
							info!("Invalid JSON \"{}\"", text);
							return;
						}
					}
				};
				let name = {
					let n = v.get(0);
					match n.map(|n| n.as_str()) {
						Some(Some(n)) => n,
						_ => {
							info!("Invalid JSON \"{}\"", text);
							return;
						}
					}
				};
				let block = {
					let b = v.get(1);
					match b {
						Some(b) => b.clone(),
						_ => {
							info!("Invalid JSON \"{}\"", text);
							return;
						}
					}
				};

				macro_rules! handle_validity {
					($t: ty) => {
						{
							let m = serde_json::from_value::<$t>(block);
							match m {
								Ok(m) => m,
								_ => {
									info!("Invalid JSON \"{}\"", text);
									return;
								}
							}
						}
					}
				};

				match name {
					"join_room" => {
						let mut msg = handle_validity!(server::JoinRoom);
						msg.ws = self.data.clone();
						self.addr.do_send(msg);
					}
					"leave_room" => {
						let mut msg = handle_validity!(server::LeaveRoom);
						msg.ws = self.data.clone();
						self.addr.do_send(msg);
					}
					"start_game" => {
						let mut msg = handle_validity!(server::StartGame);
						msg.ws = self.data.clone();
						self.addr.do_send(msg);
					}
					"change_name" => {
						let mut msg = handle_validity!(server::ChangeName);
						msg.ws = self.data.clone();
						self.addr.do_send(msg);
					}
					"play_card" => {
						let mut msg = handle_validity!(server::PlayCard);
						msg.ws = self.data.clone();
						self.addr.do_send(msg);
					}
					"draw_card" => {
						let mut msg = handle_validity!(server::DrawCard);
						msg.ws = self.data.clone();
						self.addr.do_send(msg);
					}
					"skip_card" => {
						let mut msg = handle_validity!(server::SkipCard);
						msg.ws = self.data.clone();
						self.addr.do_send(msg);
					}
					"call_uno" => {
						let mut msg = handle_validity!(server::CallUno);
						msg.ws = self.data.clone();
						self.addr.do_send(msg);
					}
					"change_house_rule" => {
						let mut msg = handle_validity!(server::ChangeHouseRule);
						msg.ws = self.data.clone();
						self.addr.do_send(msg);
					}
					_ => {
						info!("Invalid JSON \"{}\"", text);
						return;
					}
				}
			}
			ws::Message::Pong(_) => {
				//debug!("Received pong from {}", self.data.id);
				self.hb = Instant::now();
			}
			ws::Message::Close(reason) => {
				ctx.close(reason);
				ctx.stop();
			}
			_ => {}
		}
	}
}

async fn uno_route(
	req: HttpRequest, stream: web::Payload, srv: web::Data<Addr<server::UnoServer>>
) -> Result<HttpResponse, Error> {

	let resp = ws::start(
		UnoWs {
			data: UnoWsData {
				id: 0
			},
			addr: srv.get_ref().clone(),
			hb: Instant::now()
		},
		&req, stream
	);
	resp
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
	env_logger::init();

	System::current().stop_on_panic();

	let server = server::UnoServer::new().start();

	let mut settings = config::Config::default();
	settings.merge(config::File::with_name("config")).unwrap();
	settings.merge(config::Environment::new()).unwrap();
	let settings = settings.try_into::<HashMap<String, String>>().unwrap();

	let https = settings
		.get("https")
		.map(|x| x.parse::<bool>())
		.unwrap_or(Ok(false))
		.unwrap();
	let port = settings
		.get("port")
		.map(|x| x.parse::<i32>())
		.unwrap_or_else(|| Ok(if https { 8443 } else { 8080 }))
		.unwrap();
	let ip = settings
		.get("ip")
		.map(|x| x.clone())
		.unwrap_or_else(|| "127.0.0.1".into());
	let dist_path = settings
		.get("dist_path")
		.unwrap()
		.clone();

	println!("Starting with https: {}, ip: {}, port: {}, dist_path: \"{}\"", https, ip, port, dist_path);

	let serv = HttpServer::new(move || App::new()
		.wrap(Logger::default())
		.wrap(NormalizePath::default())
		.data(server.clone())
		.service(web::resource("/ws/").to(uno_route))
		.service(
			actix_files::Files::new("/", &dist_path)
				.index_file("index.html")
				.prefer_utf8(true)
		)
	);

	if https {
		let mut config = ServerConfig::new(NoClientAuth::new());
		let cert_file = &mut BufReader::new(File::open("keys/cert.pem").unwrap());
		let key_file = &mut BufReader::new(File::open("keys/key.pem").unwrap());
		let cert_chain = certs(cert_file).unwrap();
		let mut keys = pkcs8_private_keys(key_file).unwrap();
		config.set_single_cert(cert_chain, keys.remove(0)).unwrap();

		serv
			.bind_rustls(format!("{}:{}", ip, port), config)?
			.run()
			.await
	} else {
		serv
			.bind(format!("{}:{}", ip, port))?
			.run()
			.await
	}
}
