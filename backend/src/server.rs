use log::*;
use actix::prelude::*;
use maplit::*;
use rand::{self, seq::SliceRandom, rngs::ThreadRng, Rng};
use serde::{Deserialize, Serialize};
use std::collections::{HashMap, HashSet};
use super::{Card, CardColor};

#[derive(Serialize)]
pub(crate) struct ServerResponse<T: Serialize>(pub String, pub T);

#[derive(Message, Debug)]
#[rtype(result = "()")]
pub(crate) struct Message(pub String);

#[derive(Debug, Default, Copy, Clone, Serialize)]
pub(crate) struct HouseRules {
	pub(crate) draw_until_play: bool,
	pub(crate) no_end_on_wild: bool,
	pub(crate) stackable_plus: bool,
	pub(crate) seven_zero: bool
}

#[derive(Debug, Clone, Copy)]
pub(crate) struct StackTarget {
	pub(crate) stack_amount: usize,
	pub(crate) plus_four: bool
}

#[derive(Debug)]
pub(crate) struct RoomStatePlaying {
	pub(crate) player_hands: HashMap<usize, Vec<Card>>,
	pub(crate) deck: Vec<Card>,
	pub(crate) discard: Vec<Card>,
	pub(crate) cur_color: CardColor,
	pub(crate) cur_player: usize,
	pub(crate) pos_direction: bool,
	pub(crate) playable_index: Option<usize>,
	pub(crate) actions_frozen: bool,
	pub(crate) called_uno: bool,
	pub(crate) didnt_call: Option<usize>,
	pub(crate) stack_target: Option<StackTarget>
}

#[derive(Debug)]
pub(crate) enum RoomState {
	Waiting,
	Playing(RoomStatePlaying)
}

#[derive(Debug)]
pub(crate) struct Room {
	pub(crate) players: HashSet<usize>,
	pub(crate) player_order: Vec<usize>,
	pub(crate) spectators: HashSet<usize>,
	pub(crate) state: RoomState,
	pub(crate) house_rules: HouseRules,
	pub(crate) id: usize
}

impl Room {
	fn add_player(&mut self, p: usize) {
		self.players.insert(p);
		self.player_order.push(p);
	}

	fn remove_player(&mut self, p: usize) {
		self.players.remove(&p);
		self.player_order.retain(|&x| x != p);
		if let RoomState::Playing(ref mut s) = self.state {
			if let Some(v) = s.player_hands.remove(&p) {
				s.deck.extend(v.into_iter());
			}
		}
	}

	pub(crate) fn advance_player(&mut self, reset_didnt_call: bool) {
		if let RoomState::Playing(ref mut s) = self.state {
			s.cur_player += self.players.len();
			if s.pos_direction { s.cur_player += 1; } else { s.cur_player -= 1; }
			s.cur_player %= self.players.len();
			s.playable_index = None;
			s.called_uno = false;
			if reset_didnt_call { s.didnt_call = None; }
		}
	}
}

#[derive(Debug, Clone)]
pub(crate) enum InRoomState {
	NotInRoom,
	Playing(String),
	Spectating(String)
}

impl InRoomState {
	fn is_playing(&self) -> bool {
		matches!(self, Self::Playing(_))
	}
}

#[derive(Debug)]
pub(crate) struct SessionData {
	addr: Recipient<Message>,
	player_name: String,
	room: InRoomState
}

#[derive(Debug)]
pub(crate) struct UnoServer {
	pub(crate) rng: ThreadRng,
	pub(crate) sessions: HashMap<usize, SessionData>,
	pub(crate) rooms: HashMap<String, Room>,
	pub(crate) cur_room_id: std::num::Wrapping<usize>
}

impl UnoServer {
	pub(crate) fn new() -> UnoServer {
		UnoServer {
			rng: rand::thread_rng(),
			sessions: hashmap![],
			rooms: hashmap![],
			cur_room_id: std::num::Wrapping(0)
		}
	}

	fn get_game_data_response(&self, msg: GetGameData) -> Option<GetGameDataResponse> {
		info!("Sending game data to {}", msg.ws.id);

		let (spectating, room_name) = match &self.sessions[&msg.ws.id].room {
			InRoomState::Playing(r) => (false, r),
			InRoomState::Spectating(r) => (true, r),
			InRoomState::NotInRoom => {
				let iter = self.rooms.iter()
					.map(|(room_name, room_data)| RoomInfo {
						name: room_name.clone(),
						player_amount: room_data.players.len(),
						playing: matches!(room_data.state, RoomState::Playing { .. })
					});

				let rooms = iter.collect();

				return Some(GetGameDataResponse::NotInRoom { rooms });
			}
		};

		let r = match self.rooms.get(room_name) {
			Some(r) => r,
			None => {
				error!("Session {} sent GetGameData with room {} but that room doesn't exist", msg.ws.id, room_name);
				return None;
			}
		};
		if !r.players.contains(&msg.ws.id) && !r.spectators.contains(&msg.ws.id) {
			error!("Session {} sent GetGameData with room {} but that room doesn't contain that player", msg.ws.id, room_name);
			return None;
		}

		Some(
			if let RoomState::Playing(s) = &r.state {
				let client_data = if spectating {
					ClientData::Spectator
				} else {
					let player_index = r.player_order.iter().position(|&x| x == msg.ws.id).unwrap();
					let hand = s.player_hands[&msg.ws.id].clone();
					let (drawing, playable_cards) =
						if player_index == s.cur_player {
							if let Some(t) = s.stack_target {
								(
									false,
									Some(
										hand
											.iter()
											.enumerate()
											.filter(|(_, c)|
												if t.plus_four {
													matches!(c, Card::WildDrawFour { .. })
												} else {
													matches!(c, Card::Plus2 { .. })
												}
											)
											.map(|(i, _)| i)
											.collect()
									)
								)
							} else {
								match s.playable_index {
									Some(i) =>
										if UnoServer::can_play(r.house_rules, &hand, &s.discard, s.cur_color, hand[i]) {
											(true, Some(vec![i]))
										} else { (true, None) }
									None => (false, None)
								}
							}
						} else {
							(false, None)
						};

						ClientData::Player {
							can_call_uno:
								s.didnt_call.is_some() ||
								(
									player_index == s.cur_player &&
									hand.len() == 2 &&
									(
										if let Some(_) = s.playable_index {
											playable_cards.is_some()
										} else {
											hand.iter().any(|c| UnoServer::can_play(
												r.house_rules, &hand, &s.discard, s.cur_color, *c
											))
										}
									) &&
									!s.called_uno &&
									!s.actions_frozen
								),
							player_hand: hand,
							player_index,
							playable_cards,
							drawing,
						}
					};

				GetGameDataResponse::Playing {
					room_name: room_name.clone(),
					player_data: r.player_order.iter().map(|id| PlayerData {
						amount_cards: s.player_hands[id].len(),
						name: self.sessions[id].player_name.clone()
					}).collect(),
					cur_player: s.cur_player,
					top_card: s.discard[s.discard.len() - 1],
					cur_color: s.cur_color,
					pos_direction: s.pos_direction,
					client_data,
					house_rules: r.house_rules
				}
			} else {
				GetGameDataResponse::Waiting {
					room_name: room_name.clone(),
					player_names: r.player_order.iter().map(|id|
						self.sessions[id].player_name.clone()
					).collect(),
					house_rules: r.house_rules
				}
			}
		)
	}

	fn send_new_game_data_room(&self, room: &str) {
		let sessions = self.rooms[room].players.iter().chain(self.rooms[room].spectators.iter());
		for s in sessions {
			let addr = &self.sessions[s].addr;
			let game_data_req = GetGameData {
				ws: super::UnoWsData {
					id: *s
				}
			};
			let res = self.get_game_data_response(game_data_req);
			if let Some(res) = res {
				let s = serde_json::to_string(&("game_data", res)).unwrap();
				if let e @ Err(_) = addr.do_send(Message(s)) {
					error!("Actix error {:?}", e);
				}
			}
		}
	}

	fn send_new_game_data_no_room(&self) {
		'outer: for (s, SessionData { addr, .. }) in self.sessions.iter() {
			for r in self.rooms.values() {
				if r.players.contains(s) || r.spectators.contains(s) {
					continue 'outer;
				}
			}

			let game_data_req = GetGameData {
				ws: super::UnoWsData {
					id: *s
				}
			};
			let res = self.get_game_data_response(game_data_req);
			if let Some(res) = res {
				let s = serde_json::to_string(&("game_data", res)).unwrap();
				if let e @ Err(_) = addr.do_send(Message(s)) {
					error!("Actix error {:?}", e);
				}
			}
		}
	}

	fn can_play(
		house_rules: HouseRules,
		hand: &[Card], discard: &[Card],
		cur_color: CardColor, card: Card
	) -> bool {
		if house_rules.no_end_on_wild && hand.len() == 1 {
			if let Card::Wild { .. } | Card::WildDrawFour { .. } = card {
				return false;
			}
		}
		let mut ok_card = false;
		if let Some(c) = card.get_color() {
			if c == cur_color { ok_card = true; }
		}
		if discard[discard.len() - 1].type_compatible(&card) { ok_card = true; }
		return ok_card;
	}
}

#[derive(Message, Debug)]
#[rtype(usize)]
pub(crate) struct Connect {
	pub(crate) addr: Recipient<Message>
}

impl Handler<Connect> for UnoServer {
	type Result = usize;
	
	fn handle(&mut self, msg: Connect, _: &mut Context<Self>) -> Self::Result {
		let id = loop {
			let id = self.rng.gen::<usize>();

			if !self.sessions.contains_key(&id) { break id; }
		};

		info!("Connect {}", id);

		let session = SessionData { addr: msg.addr, player_name: "Player".to_string(), room: InRoomState::NotInRoom };
		self.sessions.insert(id, session);

		self.send_new_game_data_no_room();

		id
	}
}

#[derive(Message, Debug)]
#[rtype(result = "()")]
pub(crate) struct Disconnect {
	pub(crate) ws: super::UnoWsData
}

impl Handler<Disconnect> for UnoServer {
	type Result = ();
	
	fn handle(&mut self, msg: Disconnect, _: &mut Context<Self>) -> Self::Result {
		info!("Disconnect {}", msg.ws.id);

		match self.sessions[&msg.ws.id].room.clone() {
			InRoomState::Playing(room_name) => {
				match self.rooms.get_mut(&room_name) {
					Some(r) => {
						if !r.players.contains(&msg.ws.id) {
							error!("Session {} thinks it's in room {} but it isn't", msg.ws.id, room_name);
						} else {
							r.remove_player(msg.ws.id);
							match r.players.len() {
								1 => {
									r.state = RoomState::Waiting;
									for s in r.spectators.iter() {
										self.sessions.get_mut(&s).unwrap().room = InRoomState::NotInRoom;
									}
									r.spectators.clear();
									self.send_new_game_data_room(&room_name);
								}
								0 => {
									for s in r.spectators.iter() {
										self.sessions.get_mut(&s).unwrap().room = InRoomState::NotInRoom;
									}
									r.spectators.clear();
									self.rooms.remove(&room_name);
								}
								_ => {
									if let RoomState::Playing(ref mut s) = r.state {
										if s.cur_player == r.player_order.len() {
											s.cur_player = 0;
										}
										s.playable_index = None;
									}
									self.send_new_game_data_room(&room_name);
								}
							}
						}
					},
					None => {
						error!("Session {} thinks it's in room {} but no such room exists", msg.ws.id, room_name);
					}
				}
			}
			InRoomState::Spectating(room_name) => {
				match self.rooms.get_mut(&room_name) {
					Some(r) => {
						if !r.spectators.contains(&msg.ws.id) {
							error!("Session {} thinks it's in room {} but it isn't", msg.ws.id, room_name);
						} else {
							r.spectators.remove(&msg.ws.id);
						}
					},
					None => {
						error!("Session {} thinks it's in room {} but no such room exists", msg.ws.id, room_name);
					}
				}
			}
			_ => {}
		}

		self.sessions.remove(&msg.ws.id);

		self.send_new_game_data_no_room();
	}
}

#[derive(Message, Debug, Deserialize)]
#[rtype(result = "()")]
pub(crate) struct JoinRoom {
	#[serde(skip)]
	pub(crate) ws: super::UnoWsData,

	pub(crate) room_name: String
}

impl Handler<JoinRoom> for UnoServer {
	type Result = ();
	
	fn handle(&mut self, msg: JoinRoom, _: &mut Context<Self>) -> Self::Result {
		info!("{:?}", msg);

		if msg.room_name.len() == 0 { return; }

		if self.sessions[&msg.ws.id].room.is_playing() { return; }

		if !self.rooms.contains_key(&msg.room_name) {
			self.cur_room_id += std::num::Wrapping(1);
		}
		let id = self.cur_room_id.0;
		let room =
			self.rooms
				.entry(msg.room_name.clone())
				.or_insert_with(|| Room {
					players: hashset![], spectators: hashset![],
					player_order: vec![], state: RoomState::Waiting,
					house_rules: HouseRules::default(), id
				});
		
		if let RoomState::Playing(_) = room.state {
			room.spectators.insert(msg.ws.id);
			self.sessions.get_mut(&msg.ws.id).unwrap().room = InRoomState::Spectating(msg.room_name.clone());
			debug!("Rooms after join: {:?}", self.rooms);
			self.send_new_game_data_room(&msg.room_name);
			return;
		};

		room.add_player(msg.ws.id);

		debug!("Rooms after join: {:?}", self.rooms);

		self.sessions.get_mut(&msg.ws.id).unwrap().room = InRoomState::Playing(msg.room_name.clone());

		self.send_new_game_data_room(&msg.room_name);
		self.send_new_game_data_no_room();
	}
}

#[derive(Message, Debug, Deserialize)]
#[rtype(result = "()")]
pub(crate) struct LeaveRoom {
	#[serde(skip)]
	pub(crate) ws: super::UnoWsData,
}

impl Handler<LeaveRoom> for UnoServer {
	type Result = ();
	
	fn handle(&mut self, msg: LeaveRoom, _: &mut Context<Self>) -> Self::Result {
		info!("{:?}", msg);

		let room = self.sessions[&msg.ws.id].room.clone();
		match room {
			InRoomState::Playing(room_name) => {
				match self.rooms.get_mut(&room_name) {
					Some(r) => {
						if !r.players.contains(&msg.ws.id) {
							error!("Session {} thinks it's in room {} but it isn't", msg.ws.id, room_name);
						}
						if let RoomState::Playing(_) = r.state {
							return;
						}
						r.remove_player(msg.ws.id);
						self.sessions.get_mut(&msg.ws.id).unwrap().room = InRoomState::NotInRoom;
						if r.players.len() == 0 {
							self.rooms.remove(&room_name);
						} else {
							self.send_new_game_data_room(&room_name);
						}
						self.send_new_game_data_no_room();
						debug!("Rooms after leave: {:?}", self.rooms);
					},
					None => {
						error!("Session {} thinks it's in room {} but no such room exists", msg.ws.id, room_name);
					}
				}
			}
			InRoomState::Spectating(room_name) => {
				match self.rooms.get_mut(&room_name) {
					Some(r) => {
						if !r.spectators.contains(&msg.ws.id) {
							error!("Session {} thinks it's in room {} but it isn't", msg.ws.id, room_name);
						} else {
							r.spectators.remove(&msg.ws.id);
							self.sessions.get_mut(&msg.ws.id).unwrap().room = InRoomState::NotInRoom;
						}
					},
					None => {
						error!("Session {} thinks it's in room {} but no such room exists", msg.ws.id, room_name);
					}
				}
			}
			_ => {}
		}
	}
}

#[derive(Message, Debug, Deserialize)]
#[rtype(result = "()")]
pub(crate) struct ChangeName {
	#[serde(skip)]
	pub(crate) ws: super::UnoWsData,

	pub(crate) new_name: String
}

impl Handler<ChangeName> for UnoServer {
	type Result = ();

	fn handle(&mut self, msg: ChangeName, _: &mut Context<Self>) -> Self::Result {
		info!("{:?}", msg);

		if msg.new_name.len() == 0 { return; }

		self.sessions.get_mut(&msg.ws.id).unwrap().player_name = msg.new_name;

		if let InRoomState::Playing(room) = &self.sessions[&msg.ws.id].room {
			self.send_new_game_data_room(&room);
		}
	}
}

#[derive(Debug, Clone, Serialize)]
pub(crate) struct RoomInfo {
	name: String,
	player_amount: usize,
	playing: bool
}

macro_rules! get_room {
	($self: ident, $msg: ident, $type: expr) => {
		{
			let room_name = match &$self.sessions[&$msg.ws.id].room {
				InRoomState::Playing(r) => r.clone(),
				InRoomState::Spectating(_) => { return; }
				InRoomState::NotInRoom => { return; }
			};
			let r = match $self.rooms.get_mut(&room_name) {
				Some(r) => r,
				None => {
					error!("Session {} sent {} with room {} but that room doesn't exist", $type, $msg.ws.id, room_name);
					return;
				}
			};
			if !r.players.contains(&$msg.ws.id) {
				error!("Session {} sent {} with room {} but that room doesn't contain that player", $type, $msg.ws.id, room_name);
				return;
			}
			
			(room_name, r)
		}
	}
}

#[derive(Message, Debug, Deserialize)]
#[rtype(result = "()")]
pub(crate) struct StartGame {
	#[serde(skip)]
	pub(crate) ws: super::UnoWsData,
}

impl Handler<StartGame> for UnoServer {
	type Result = ();
	
	fn handle(&mut self, msg: StartGame, _: &mut Context<Self>) -> Self::Result {
		info!("StartGame {}", msg.ws.id);

		let (room_name, r) = get_room!(self, msg, "StartGame");

		if let RoomState::Playing(_) = r.state {
			return;
		}

		if r.players.len() == 1 { return; }

		r.player_order.shuffle(&mut self.rng);

		let mut deck = super::get_deck(&mut self.rng);
		let mut discard = vec![];
		let top_card = loop {
			match deck.pop().unwrap() {
				c @ Card::Wild { .. } | c @ Card::WildDrawFour { .. } => {
					let new_index = self.rng.gen_range(0..deck.len());
					deck.insert(new_index, c);
				}
				c => { break c; }
			}
		};
		discard.push(top_card);

		let start_num = 7.min(deck.len() / r.players.len());
		if start_num == 0 { return; }

		let mut player_hands = hashmap![];
		for p in r.players.iter() {
			let mut hand = vec![];
			super::deal(&mut deck, &mut discard, &mut hand, start_num, &mut self.rng);
			player_hands.insert(*p, hand);
		}

		r.state = RoomState::Playing(RoomStatePlaying {
			deck,
			discard,
			player_hands,
			cur_color: top_card.get_color().unwrap(),
			cur_player: 0,
			pos_direction: true,
			playable_index: None,
			actions_frozen: false,
			called_uno: false,
			didnt_call: None,
			stack_target: None
		});

		top_card.apply_card_action(r, &mut self.rng, None);

		debug!("Room state after starting game: {:#?}", r);

		self.send_new_game_data_room(&room_name);
		self.send_new_game_data_no_room();
	}
}

#[derive(Debug, Deserialize)]
pub(crate) struct GetGameData {
	#[serde(skip)]
	pub ws: super::UnoWsData,
}

#[derive(Debug, Clone, Serialize)]
pub(crate) struct PlayerData {
	amount_cards: usize,
	name: String
}

#[derive(Debug, Clone, Serialize)]
#[serde(tag = "client_type")]
pub(crate) enum ClientData {
	Player {
		player_hand: Vec<Card>,
		playable_cards: Option<Vec<usize>>,
		player_index: usize,
		drawing: bool,
		can_call_uno: bool,
	},
	Spectator
}

#[derive(Debug, Clone, Serialize)]
#[serde(tag = "type")]
pub(crate) enum GetGameDataResponse {
	NotInRoom {
		rooms: Vec<RoomInfo>
	},
	Waiting {
		room_name: String,
		player_names: Vec<String>,
		house_rules: HouseRules
	},
	Playing {
		room_name: String,
		player_data: Vec<PlayerData>,
		cur_player: usize,
		top_card: Card,
		cur_color: CardColor,
		pos_direction: bool,
		#[serde(flatten)]
		client_data: ClientData,
		house_rules: HouseRules
	}
}

#[derive(Message, Debug, Deserialize)]
#[rtype(result = "()")]
pub(crate) struct PlayCard {
	#[serde(skip)]
	pub(crate) ws: super::UnoWsData,

	pub(crate) index: usize,
	pub(crate) wild_color: Option<CardColor>,
	pub(crate) seven_target: Option<usize>
}

#[derive(Debug, Serialize)]
pub(crate) struct CardPlayedEvent {
	pub(crate) card: Card,
	pub(crate) wild_color: Option<CardColor>
}

#[derive(Debug, Serialize)]
pub(crate) struct UnoCalledEvent {
	pub(crate) who: usize
}

#[derive(Debug, Serialize)]
pub(crate) struct GameEndedEvent {
	winner_index: usize,
	your_index: usize,
	scores: Vec<(String, u32)>
}

impl Handler<PlayCard> for UnoServer {
	type Result = ();
	
	fn handle(&mut self, msg: PlayCard, _: &mut Context<Self>) -> Self::Result {
		info!("{:?}", msg);

		let (room_name, r) = get_room!(self, msg, "PlayCard");

		if let RoomState::Playing(ref mut s) = r.state {
			if s.actions_frozen { return; }

			if msg.ws.id != r.player_order[s.cur_player] { return; }
			let hand = s.player_hands.get_mut(&msg.ws.id).unwrap();

			if let Some(i) = s.playable_index {
				if msg.index != i { return; }
			}

			let card = hand.get(msg.index);
			if card.is_none() { return; }
			let card = *card.unwrap();

			if let Some(t) = s.stack_target {
				if t.plus_four {
					match card {
						Card::WildDrawFour { .. } => {}
						_ => { return; }
					}
				} else {
					match card {
						Card::Plus2 { .. } => {}
						_ => { return; }
					}
				}
			}

			if !UnoServer::can_play(r.house_rules, &hand, &s.discard, s.cur_color, card) { return; }

			hand.remove(msg.index);
			s.discard.push(card);
			s.cur_color = card.get_color().unwrap_or_else(|| {
				msg.wild_color.unwrap_or_else(|| {
					error!("Client sent no color with a wild card");
					CardColor::Red
				})
			});

			for s in r.players.iter().chain(r.spectators.iter()) {
				let addr = &self.sessions[s].addr;
				let e = CardPlayedEvent { card, wild_color: {
						match card.get_color() {
							Some(_) => None,
							None => Some(msg.wild_color.unwrap_or(CardColor::Red))
						}
					}
				};
				let s = serde_json::to_string(&("card_played_event", e)).unwrap();
				if let e @ Err(_) = addr.do_send(Message(s)) {
					error!("Actix error {:?}", e);
				}
			}

			let mut reset_didnt_call = true;
			match hand.len() {
				0 => {
					let hands = s.player_hands.clone();
					let order = r.player_order.clone();
					for s in r.spectators.iter() {
						self.sessions.get_mut(&s).unwrap().room = InRoomState::NotInRoom;
					}
					r.spectators.clear();

					let winner_index = s.cur_player;
					r.state = RoomState::Waiting;

					let scores: Vec<_> = order.iter().map(|i|
						(self.sessions[i].player_name.clone(), super::score(&hands[i]))
					).collect();

					self.send_new_game_data_room(&room_name);
					self.send_new_game_data_no_room();
					for (i, id) in order.iter().enumerate() {
						let e = GameEndedEvent {
							winner_index,
							your_index: i,
							scores: scores.clone()
						};
						let s = serde_json::to_string(&("game_ended_event", e)).unwrap();
						if let e @ Err(_) = self.sessions[id].addr.do_send(Message(s)) {
							error!("Actix error {:?}", e);
						}
					}
					return;
				}
				1 => {
					if !s.called_uno {
						s.didnt_call = Some(s.cur_player);
						reset_didnt_call = false;
					} else {
						for sp in r.players.iter().chain(r.spectators.iter()) {
							let addr = &self.sessions[sp].addr;
							let e = UnoCalledEvent { who: s.cur_player };
							let s = serde_json::to_string(&("uno_called_event", e)).unwrap();
							if let e @ Err(_) = addr.do_send(Message(s)) {
								error!("Actix error {:?}", e);
							}
						}
					}
				}
				_ => {}
			}

			let player = s.cur_player;

			r.advance_player(reset_didnt_call);

			card.apply_card_action(r, &mut self.rng, Some((player, msg)));

			debug!("Room state after card played: {:#?}", r.state);

			self.send_new_game_data_room(&room_name);
		}
	}
}

#[derive(Message, Debug, Deserialize)]
#[rtype(result = "()")]
pub(crate) struct DrawCard {
	#[serde(skip)]
	pub(crate) ws: super::UnoWsData,
}

impl Handler<DrawCard> for UnoServer {
	type Result = ();
	
	fn handle(&mut self, msg: DrawCard, ctx: &mut Context<Self>) -> Self::Result {
		info!("{:?}", msg);

		let (room_name, r) = get_room!(self, msg, "DrawCard");

		if let RoomState::Playing(ref mut s) = r.state {
			if s.actions_frozen { return; }

			if msg.ws.id != r.player_order[s.cur_player] { return; }
			let hand = s.player_hands.get_mut(&msg.ws.id).unwrap();

			if let Some(i) = s.playable_index {
				if UnoServer::can_play(r.house_rules, &hand, &s.discard, s.cur_color, hand[i]) {
					return;
				}
			}

			if let Some(t) = s.stack_target {
				super::deal(
					&mut s.deck, &mut s.discard, hand,
					t.stack_amount * if t.plus_four { 4 } else { 2 },
					&mut self.rng
				);
				s.stack_target = None;
				r.advance_player(false);
				self.send_new_game_data_room(&room_name);
				return;
			}

			super::deal(&mut s.deck, &mut s.discard, hand, 1, &mut self.rng);
			s.playable_index = Some(hand.len() - 1);

			let card = hand[hand.len() - 1];
			if 
				!UnoServer::can_play(r.house_rules, &hand, &s.discard, s.cur_color, card) &&
				!r.house_rules.draw_until_play
			{
				let room_name = room_name.clone();
				let delay = self.rng.gen_range(1000..=1200);
				let before_id = r.id;
				s.actions_frozen = true;
				// this is delayed to prevent a minor form of metagaming
				// where other players can distinguish between "couldn't play"
				// and "didn't play", by looking at how long the game took
				// to move to the next player
				ctx.run_later(std::time::Duration::from_millis(delay),
					move |act, _| if let Some(r) = act.rooms.get_mut(&room_name) {
						if r.id != before_id { return; }

						if let RoomState::Playing(ref mut s) = r.state {
							s.actions_frozen = false;
						}
						r.advance_player(true);

						act.send_new_game_data_room(&room_name);
					}
				);
			}

			debug!("Room state after card drawn: {:#?}", r.state);

			self.send_new_game_data_room(&room_name);
		}
	}
}

#[derive(Message, Debug, Deserialize)]
#[rtype(result = "()")]
pub(crate) struct SkipCard {
	#[serde(skip)]
	pub(crate) ws: super::UnoWsData,
}

impl Handler<SkipCard> for UnoServer {
	type Result = ();
	
	fn handle(&mut self, msg: SkipCard, _: &mut Context<Self>) -> Self::Result {
		info!("{:?}", msg);

		let (room_name, r) = get_room!(self, msg, "SkipCard");

		if let RoomState::Playing(ref mut s) = r.state {
			if s.actions_frozen { return; }

			if msg.ws.id != r.player_order[s.cur_player] { return; }

			let hand = &s.player_hands[&msg.ws.id];
			if let Some(i) = s.playable_index {
				if UnoServer::can_play(r.house_rules, &hand, &s.discard, s.cur_color, hand[i]) {
					r.advance_player(true);
				}
			}

			debug!("Room state after card not played: {:#?}", r.state);

			self.send_new_game_data_room(&room_name);
		}
	}
}

#[derive(Message, Debug, Deserialize)]
#[rtype(result = "()")]
pub(crate) struct CallUno {
	#[serde(skip)]
	pub(crate) ws: super::UnoWsData,
}

impl Handler<CallUno> for UnoServer {
	type Result = ();
	
	fn handle(&mut self, msg: CallUno, _: &mut Context<Self>) -> Self::Result {
		info!("{:?}", msg);

		let (room_name, r) = get_room!(self, msg, "SkipCard");

		if let RoomState::Playing(ref mut s) = r.state {
			if s.actions_frozen { return; }

			if let Some(i) = s.didnt_call {
				let p = r.player_order[i];
				if p != msg.ws.id {
					let hand = s.player_hands.get_mut(&p).unwrap();
					super::deal(&mut s.deck, &mut s.discard, hand, 2, &mut self.rng);
				}
				s.didnt_call = None;
				for s in r.players.iter().chain(r.spectators.iter()) {
					let addr = &self.sessions[s].addr;
					let e = UnoCalledEvent {
						who: r.player_order.iter().position(|&x| x == msg.ws.id).unwrap()
					};
					let s = serde_json::to_string(&("uno_called_event", e)).unwrap();
					if let e @ Err(_) = addr.do_send(Message(s)) {
						error!("Actix error {:?}", e);
					}
				}
			} else if msg.ws.id == r.player_order[s.cur_player] {
				s.called_uno = true;
			}

			debug!("Room state after Uno called: {:#?}", r.state);

			self.send_new_game_data_room(&room_name);
		}
	}
}

#[derive(Message, Debug, Deserialize)]
#[rtype(result = "()")]
pub(crate) struct ChangeHouseRule {
	#[serde(skip)]
	pub(crate) ws: super::UnoWsData,

	pub(crate) rule_name: String,
	pub(crate) new_value: bool
}

impl Handler<ChangeHouseRule> for UnoServer {
	type Result = ();
	
	fn handle(&mut self, msg: ChangeHouseRule, _: &mut Context<Self>) -> Self::Result {
		info!("{:?}", msg);

		let (room_name, r) = get_room!(self, msg, "ChangeHouseRule");

		if let RoomState::Playing(_) = r.state { return; }

		match msg.rule_name.as_str() {
			"draw_until_play" => r.house_rules.draw_until_play = msg.new_value,
			"no_end_on_wild" => r.house_rules.no_end_on_wild = msg.new_value,
			"stackable_plus" => r.house_rules.stackable_plus = msg.new_value,
			"seven_zero" => r.house_rules.seven_zero = msg.new_value,
			_ => {}
		}

		self.send_new_game_data_room(&room_name);
	}
}

impl Actor for UnoServer {
	type Context = Context<Self>;
}
