#!/usr/bin/env python3
import argparse
import asyncio
import signal
import sys
import time
import json

import aiohttp

queue = asyncio.Queue()

async def start_client(url, loop):
    ws = await aiohttp.ClientSession().ws_connect(url, autoclose=False, autoping=False)

    def stdin_callback():
        line = sys.stdin.buffer.readline().decode('utf-8')
        if not line:
            loop.stop()
        else:
            line = line.rstrip("\n")
            s = line.split(" ")
            if s[0] == "join":
                j = json.dumps(["join_room", { "room_name": s[1] }])
                asyncio.ensure_future(queue.put(ws.send_str(j)))
            if s[0] == "leave":
                j = json.dumps(["leave_room", {}])
                asyncio.ensure_future(queue.put(ws.send_str(j)))
            if s[0] == "name":
                j = json.dumps(["change_name", { "new_name": s[1] }])
                asyncio.ensure_future(queue.put(ws.send_str(j)))
            if s[0] == "start":
                j = json.dumps(["start_game", {}])
                asyncio.ensure_future(queue.put(ws.send_str(j)))
            if s[0] == "play":
                wild_color = s[2] if 2 < len(s) else None
                j = json.dumps(["play_card", { "index": int(s[1]), "wild_color": wild_color}])
                asyncio.ensure_future(queue.put(ws.send_str(j)))
            if s[0] == "draw":
                j = json.dumps(["draw_card", {}])
                asyncio.ensure_future(queue.put(ws.send_str(j)))
            if s[0] == "list":
                j = json.dumps(["list_rooms", {}])
                asyncio.ensure_future(queue.put(ws.send_str(j)))

    loop.add_reader(sys.stdin, stdin_callback)

    async def dispatch():
        while True:
            msg = await ws.receive()
            if msg.type == aiohttp.WSMsgType.TEXT:
                j = json.loads(msg.data.strip())
                if j[0] == "game_data":
                    d = j[1]

                    if d["type"] == "Playing":
                        print("Your cards are:")
                        for c in enumerate(d["player_hand"]):
                            print(c)

                        print()

                        top_card = d["top_card"]
                        print("The top card is", top_card)
                        if top_card["type"] == "Wild" or top_card["type"] == "WildDrawFour":
                            print("The wildcard color is", d["cur_color"])

                        print()

                        print("Players:")
                        p = d["player_data"]
                        for i in range(len(p)):
                            index = (i + d["player_index"]) % len(p)
                            player = p[index]
                            s = player["name"]
                            if i == 0: s += " (You)"
                            s += ": {} cards".format(player["amount_cards"])
                            if index == d["cur_player"]: s += " (Playing)"
                            print(s)

                        print()

                    elif d["type"] == "Waiting":
                        print("Waiting for game to start")
                        print("Players:")

                        for s in d["player_names"]:
                            print(s)

                    elif d["type"] == "NotInRoom":
                        rooms = d["rooms"]
                        print("Rooms:")
                        for r in rooms:
                            print("{}: {} player(s)".format(r["name"], r["player_amount"]))

                elif j[0] == "list_rooms_response":
                    d = j[1]
                    print("Rooms:")
                    for r in d:
                        print("{}: {} player(s)".format(r["name"], r["player_amount"]))

                    print()

                else:
                    print(j)

            elif msg.type == aiohttp.WSMsgType.BINARY:
                print('Binary: ', msg.data)
            elif msg.type == aiohttp.WSMsgType.PING:
                await ws.pong()
            elif msg.type == aiohttp.WSMsgType.PONG:
                print('Pong received')
            else:
                if msg.type == aiohttp.WSMsgType.CLOSE:
                    await ws.close()
                elif msg.type == aiohttp.WSMsgType.ERROR:
                    print('Error during receive %s' % ws.exception())
                elif msg.type == aiohttp.WSMsgType.CLOSED:
                    pass
                break

    await dispatch()


async def tick():
    while True:
        await (await queue.get())


async def main(url, loop):
    task0 = asyncio.create_task(start_client(url, loop))
    task1 = asyncio.create_task(tick())
    await asyncio.wait([task0, task1])


ARGS = argparse.ArgumentParser(
    description="websocket console client for wssrv.py example.")
ARGS.add_argument(
    '--host', action="store", dest='host',
    default='127.0.0.1', help='Host name')
ARGS.add_argument(
    '--port', action="store", dest='port',
    default=8443, type=int, help='Port number')

if __name__ == '__main__':
    args = ARGS.parse_args()
    if ':' in args.host:
        args.host, port = args.host.split(':', 1)
        args.port = int(port)

    url = 'https://{}:{}/ws/'.format(args.host, args.port)

    loop = asyncio.get_event_loop()
    loop.add_signal_handler(signal.SIGINT, loop.stop)
    asyncio.Task(main(url, loop))
    loop.run_forever()
