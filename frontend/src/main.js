import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

Vue.config.productionTip = false

import Fitty from "vue-fitty";
Vue.use(Fitty);

new Vue({
	router,
	render: h => h(App),
	store,

	mounted() {
		this.$router.replace('/notinroom')
	}
}).$mount('#app')
