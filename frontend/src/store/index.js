import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
	state: {
		connection: null,
		game_data: null,
		player_name: "",
		dark: false,
		mobile: false
	},
	mutations: {
		set_connection (state, connection) {
			state.connection = connection;
		},
		set_data (state, new_data) {
			state.game_data = new_data;
		},
		set_player_name (state, player_name) {
			state.player_name = player_name;
		},
		set_dark(state, dark) {
			state.dark = dark;
		},
		set_mobile(state, mobile) {
			state.mobile = mobile;
		}
	},
	actions: {
	},
	modules: {
	}
})
