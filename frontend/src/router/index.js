import Vue from 'vue'
import VueRouter from 'vue-router'
import Meta from 'vue-meta'

import NotInRoom from '../views/NotInRoom.vue'

const Waiting = () => import(/* webpackChunkName: "group-waiting" */ '../views/Waiting.vue')
const Playing = () => import(/* webpackChunkName: "group-playing" */ '../views/Playing.vue')

Vue.use(VueRouter)
Vue.use(Meta)

const routes = [
	{
		path: '/notinroom',
		name: 'NotInRoom',
		component: NotInRoom
	},
	{
		path: '/waiting',
		name: 'Waiting',
		component: Waiting
	},
	{
		path: '/playing',
		name: 'Playing',
		component: Playing
	}
]

const router = new VueRouter({
	routes,
	mode: 'abstract'
})

export default router
