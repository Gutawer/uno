# syntax=docker/dockerfile:experimental

FROM rust:1.44.1 as backend

	WORKDIR /backend
	COPY ./backend/Cargo.toml .
	COPY ./backend/Cargo.lock .
	COPY ./backend/src ./src

	RUN \
		--mount=type=cache,target=/usr/local/cargo/registry \
		--mount=type=cache,target=/backend/target \
		cargo install --path .

FROM node:15 as frontend

	WORKDIR /frontend
	COPY ./frontend/package.json .
	COPY ./frontend/vue.config.js .
	COPY ./frontend/babel.config.js .
	COPY ./frontend/yarn.lock .
	COPY ./frontend/public ./public
	COPY ./frontend/src ./src

	ARG WS_URL

	RUN touch .env.local && printf "VUE_APP_WEBSOCKET_URL=%s\n" $WS_URL > .env.local
	RUN \
		--mount=type=cache,target=/frontend/node_modules \
		yarn install
	RUN \
		--mount=type=cache,target=/frontend/node_modules \
		yarn build

FROM debian:stable-slim

	ARG HTTPS=false
	ARG IP=127.0.0.1
	ARG PORT=8080
	RUN touch config.toml
	RUN printf "https = %s\n" $HTTPS >> config.toml
	RUN printf "ip = \"%s\"\n" $IP >> config.toml
	RUN printf "port = %s\n" $PORT >> config.toml
	RUN printf "dist_path = \"dist/\"\n" >> config.toml

	COPY --from=frontend /frontend/dist ./dist
	COPY --from=backend /usr/local/cargo/bin/guta_uno /bin

	EXPOSE $port

	CMD ["guta_uno"]
