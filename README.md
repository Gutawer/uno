# Hacksoc Uno

### Dependencies

`cargo` and `yarn` must be on the system.

### Build instructions

The project consists of two parts, the `backend` and `frontend`.

`docker` can be used to build the project, using the `Dockerfile`.

* The `Dockerfile` takes the following args:

  - `WS_URL`: String, non-optional. Contains the websocket URL. See
    `VUE_APP_WEBSOCKET_URL` below.

  - `HTTPS`: Boolean, optional. Contains whether the project will use
    rustls, see `https` below.

  - `IP`: String, optional. Contains the IP to be bound to. See `ip`
    below.

  - `PORT`: Integer, optional. Contains the port to be used. See `port`
    below.

* Example command:
  ```
  docker build -t uno . --build-arg WS_URL="wss://localhost:8443/ws/" --build_arg PORT=8443
  docker run --net host uno
  ```

Otherwise, building is as follows:

#### `backend`:
- Run `cargo build --release` in the `backend/` folder.

#### `frontend`:

* Run `yarn install` in the `frontend/` folder to install dependencies.

* Create a file `.env.local` in the `frontend/` folder. This will contain
  environment variables that the frontend needs.

* Add a line `VUE_APP_WEBSOCKET_URL=<url>` into `.env.local`, where
  `<url>` is the URL that the client will connect to for communication.
  This will be something like `wss://url.path/ws/`.

* Run `yarn build` in the `frontend/` folder.

### Running

* The `backend` requires a configuration file in `backend/` called `config.toml`.
  This contains the following attributes.
  All attributes can be overrided by environment variables.

  - `https` is a bool, and controls whether the backend will serve things
    over https itself. If enabled, a `keys/` folder will also be looked
    for which should contain a `cert.pem` file and a `key.pem` file.
    Defaults to `false`.

  - `ip` is a string, and is the ip and websocket will be bound to. Defaults
    to `"127.0.0.1"`.

  - `port` is an integer, and is the port the website and websocket will
    be served on. Defaults to `8443` for `https`, and `8080` for
    `!https`.

  - `dist_path` is a string, and is a path to the `dist/` folder that `yarn build`
    creates. Non-optional, since the program doesn't know where this is relative to
	the working directory. `"../frontend/dist/"` will work if the server is ran from
	the `backend/` folder.

  - For example:
    ```
    https = false
    ip = "127.0.0.1"
    port = 8080
    dist_path = "../frontend/dist/"
    ```

* Once created, `cargo run --release` will run the server on the specified IP and port.
  It will serve the websocket at `/ws/`, look for the `yarn`-generated `dist/` folder
  in `dist_path`, and serve that at `/`.
